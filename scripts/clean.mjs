import { cleanDesktopBuild } from './utils/jobs.mjs';

async function main() {
  await cleanDesktopBuild();
}

main();
